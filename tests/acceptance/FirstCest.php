<?php

use AspectMock\Test as test;
use src\Api;

class FirstCest
{
    public function frontpageWorks(AcceptanceTester $I)
    {

        $apiCallResponses = [
            // url => json response
            'someUrl' => ['success' => 1, 'data' => 'mock']
        ];

        $I->amOnPage('/?mock_responses=' . json_encode($apiCallResponses));

        $I->see('mocktest');
    }
}
