<?php

namespace src;

class Api {
    function apiCall($url)
    {

        $isDevelopment = true; // get this value from some config in real project

        if (isset($_GET['mock_responses']) && $isDevelopment) {
            // can improve by adding matching not only by url, but by HTTP verb.
            $responseByUrl = json_decode($_GET['mock_responses'], true)[$url];

            return json_encode($responseByUrl);
        }

        $apiCallResponse = ['success' => 1, 'data' => 'real'];
        return json_encode($apiCallResponse);
    }
}
